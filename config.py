import redis


class Config(object):
    DEBUG = None
    #  建立数据库连接
    SQLALCHEMY_DATABASE_URI = 'mysql://root:mysql@localhost/project'
    SQLACHEMY_TRACK_MODIFICATIONS = True

    # 配置secret_key
    SECRET_KEY = '1PHEj1mez+fhQV+hmTLMrZuK69s='

    # 指定sessions保存到redis数据库中
    SESSION_TYPE = 'redis'
    # 让cookie中的session被签名加密处理
    SESSION_USE_SIGNER = True
    # 使用redis实例化对象
    SESSION_HOST = 'localhost'
    # 定义一个变量存储固定的地址和端口，复用
    SESSION_PORT = 6379
    SESSION_REDIS = redis.StrictRedis(host=SESSION_HOST, port=SESSION_PORT, db=0)
    # 设置session的过期时间
    PERMANENT_SESSION_LIFETIME = 86400


class Production(Config):
    DEBUG = False


class Develop(Config):
    DEBUG = True

config = {
    'production': Production,
    'develop': Develop
}