from flask import Blueprint

register_blue = Blueprint('register_blue', __name__, url_prefix='/register')
from info.modules.register import views
