# 自定义一个过滤器，用来展示点击排行的前三的效果
import functools

from flask import current_app
from flask import g
from flask import session

from info.models import User


def index_news(index):
    # 判断循环的次数，第一次到第三次的时候显示特殊效果
    if index == 1:
        return 'first'
    elif index == 2:
        return 'second'
    elif index == 3:
        return 'third'
    else:
        return ''


# 定义一个装饰器用来验证用户的登录状态,自动获取用户的登录信息
# 因为使用了装饰器，得到的函数名称都是相同的，需要用到另一个装饰器改变得到的每一个函数名称
def user_landing(f):
    @functools.wraps(f)
    def wrapper(*args, **kwargs):
        user_id = session.get('user_id')
        user = None
        # 判断用户id是否存在
        if user_id:
            # 从数据库中获取指定用户id的用户数据，放入异常捕获中
            try:
                user = User.query.get(user_id)
            except Exception as e:
                current_app.logger.error(e)
        # 将用户信息临时保存在g变量中
        g.user = user
        return f(*args, **kwargs)

    return wrapper
