import logging
from logging.handlers import RotatingFileHandler

import redis
from flask import Flask
# 导入数据库模块
from flask.ext.wtf import CSRFProtect, csrf
from flask_sqlalchemy import SQLAlchemy
# 导入session模块，将session保存在redis数据库中
from flask_session import Session
from config import Config, config

# 设置日志的记录等级
logging.basicConfig(level=logging.DEBUG)  # 调试debug级
# 创建日志记录器，指明日志保存的路径、每个日志文件的最大大小、保存的日志文件个数上限
file_log_handler = RotatingFileHandler("logs/log", maxBytes=1024 * 1024 * 100, backupCount=10)
# 创建日志记录的格式 日志等级 输入日志信息的文件名 行数 日志信息
formatter = logging.Formatter('%(levelname)s %(filename)s:%(lineno)d %(message)s')
# 为刚创建的日志记录器设置日志记录格式
file_log_handler.setFormatter(formatter)
# 为全局的日志工具对象（flask app使用的）添加日志记录器
logging.getLogger().addHandler(file_log_handler)

db = SQLAlchemy()
# 实例化redis数据库用来存储前段发送过来的图片验证码的内容
sr_redis = redis.StrictRedis(host=Config.SESSION_HOST, port=Config.SESSION_PORT, decode_responses=True)


# 定义一个工厂函数，调用时才生效,接收到参数，并调用config中定义的字典中
def create_app(config_name):
    app = Flask(__name__)
    # 导入config文件中的Config类
    app.config.from_object(config[config_name])
    # 实例化数据库 project
    db.init_app(app)
    # 实例化session
    Session(app)
    # 开启跨站请求保护
    CSRFProtect(app)

    # 生成csrf_token并传入浏览器中，但是每次有用户访问的时候都要生成csrf_token，需要用到请求钩子
    @app.after_request
    def csrf_token(response):
        # 生成crsf_token令牌
        crsf_token = csrf.generate_csrf()
        # 将令牌存入到浏览器的cookie中
        response.set_cookie('crsf_token', crsf_token)
        # 注册以及登录的时候都需要验证
        return response

    # 导入自定义的过滤器
    from info.utils.common import index_news
    app.add_template_filter(index_news, 'index_news')

    # 导入创建的蓝图对象注册蓝图
    from info.modules.news import new_views
    app.register_blueprint(new_views)
    # 导入创建的用户注册蓝图对象注册对象
    from info.modules.register import register_blue
    app.register_blueprint(register_blue)
    return app
