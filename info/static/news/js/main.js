$(function () {

    // 打开登录框
    $('.login_btn').click(function () {
        $('.login_form_con').show();
    }),

    // 点击关闭按钮关闭登录框或者注册框
    $('.shutoff').click(function () {
        $(this).closest('form').hide();
    }),

    // 隐藏错误
    $(".login_form #mobile").focus(function () {
        $("#login-mobile-err").hide();
    });
    $(".login_form #password").focus(function () {
        $("#login-password-err").hide();
    });

    $(".register_form #mobile").focus(function () {
        $("#register-mobile-err").hide();
    });
    $(".register_form #imagecode").focus(function () {
        $("#register-image-code-err").hide();
    });
    $(".register_form #smscode").focus(function () {
        $("#register-sms-code-err").hide();
    });
    $(".register_form #password").focus(function () {
        $("#register-password-err").hide();
    });


    // 点击输入框，提示文字上移
    $('.form_group').on('click focusin', function () {
        $(this).children('.input_tip').animate({
            'top': -5,
            'font-size': 12
        }, 'fast').siblings('input').focus().parent().addClass('hotline');
    })

    // 输入框失去焦点，如果输入框为空，则提示文字下移
    $('.form_group input').on('blur focusout', function () {
        $(this).parent().removeClass('hotline');
        var val = $(this).val();
        if (val == '') {
            $(this).siblings('.input_tip').animate({'top': 22, 'font-size': 14}, 'fast');
        }
    })


    // 打开注册框
    $('.register_btn').click(function () {
        $('.register_form_con').show();
        generateImageCode()
    })


    // 登录框和注册框切换
    $('.to_register').click(function () {
        $('.login_form_con').hide();
        $('.register_form_con').show();
        generateImageCode()
    });

    // 登录框和注册框切换
    $('.to_login').click(function () {
        $('.login_form_con').show();
        $('.register_form_con').hide();
    });

    // 根据地址栏的hash值来显示用户中心对应的菜单
    var sHash = window.location.hash;
    if (sHash != '') {
        var sId = sHash.substring(1);
        var oNow = $('.' + sId);
        var iNowIndex = oNow.index();
        $('.option_list li').eq(iNowIndex).addClass('active').siblings().removeClass('active');
        oNow.show().siblings().hide();
    }

    // 用户中心菜单切换
    var $li = $('.option_list li');
    var $frame = $('#main_frame');

    $li.click(function () {
        if ($(this).index() == 5) {
            $('#main_frame').css({'height': 900});
        }
        else {
            $('#main_frame').css({'height': 660});
        }
        $(this).addClass('active').siblings().removeClass('active');
        $(this).find('a')[0].click()
    })

    // TODO 登录表单提交
    $(".login_form_con").submit(function (e) {
        e.preventDefault();
        var mobile = $(".login_form #mobile").val();
        var password = $(".login_form #password").val();

        if (!mobile) {
            $("#login-mobile-err").show();
            return;
        }

        if (!password) {
            $("#login-password-err").show();
            return;
        }

        // 发起登录请求
        var landing_info ={
            'mobile':mobile,
            'password':password
        };
        $.ajax({
            url:'/register/landing',
            type:'post',
            dataType:'json',
            data:JSON.stringify(landing_info),
            contentType:'application/json',
            headers:{
                'X-CSRFToken':getCookie('crsf_token')
            },
            success:function(response) {
                if(response.erron=='0'){
                    location.reload()

                }else{
                    $('#login-password-err').html(response.error_info);
                    $('#login-password-err').show()
                }

            }

        })


    })


    // TODO 注册按钮点击
    $(".register_form_con").submit(function (e) {
        // 阻止默认提交操作
        e.preventDefault()

        // 取到用户输入的内容
        var mobile = $("#register_mobile").val()
        var smscode = $("#smscode").val()
        var password = $("#register_password").val()

        if (!mobile) {
            $("#register-mobile-err").show();
            return;
        }
        if (!smscode) {
            $("#register-sms-code-err").show();
            return;
        }
        if (!password) {
            $("#register-password-err").html("请填写密码!");
            $("#register-password-err").show();
            return;
        }

        if (password.length < 6) {
            $("#register-password-err").html("密码长度不能少于6位");
            $("#register-password-err").show();
            return;
        }

        // 发起注册请求
        var register_info={
            'mobile':mobile,
            'smscode':smscode,
            'password':password
        };
        $.ajax({
            url:'/register/info',
            type:'post',
            data:JSON.stringify(register_info),
            contentType:'application/json',
            dataType:'json',
            // 设置crsf_token防止跨站伪造访问
            headers:{
                'X-CSRFToken':getCookie('crsf_token')
            },
            success:function (response) {
                if(response.erron=='0'){
                    location.reload()
                }else{
                    $('#register-password-err').html(response.error_info);
                    $('#register-password-err').show()

                }
            }
        })
    })
});

var imageCodeId = "";

// TODO 生成一个图片验证码的编号，并设置页面中图片验证码img标签的src属性
function generateImageCode() {
    // 使用UUID生成一个编号，并拼接到路由地址上
    imageCodeId = generateUUID();
    // 将生成的图片验证码拼接到请求的路由地址上
    var image_url = '/register/image_code?code_id=' + imageCodeId;
    // 点击事件触发后更新图片，并将拼接好的图片的地址连同设置的图片编号一起发送给服务器
    $('.get_pic_code').attr('src',image_url)

}

// 发送短信验证码
function sendSMSCode() {
    // 校验参数，保证输入框有数据填写
    $(".get_code").removeAttr("onclick");
    var mobile = $("#register_mobile").val();
    if (!mobile) {
        $("#register-mobile-err").html("请填写正确的手机号！");
        $("#register-mobile-err").show();
        $(".get_code").attr("onclick", "sendSMSCode();");
        return;
    }
    var imageCode = $("#imagecode").val();
    if (!imageCode) {
        $("#image-code-err").html("请填写验证码！");
        $("#image-code-err").show();
        $(".get_code").attr("onclick", "sendSMSCode();");
        return;
    }

    // TODO 发送短信验证码
    // 声明一个对象，将需要验证的数据存储在对象中
    var register_info={
        'mobile':mobile,
        'image_text':imageCode,
        'image_code_id':imageCodeId
    };
    // 通过ajax实现前后端数据的交互
    $.ajax({
        // 请求的网址
        url:'/register/send_message',
        // 请求的方法
        type:'post',
        // 请求的内容
        data:JSON.stringify(register_info),
        // 请求返回的数据格式
        dataType: 'json',
        // 获得请求的服务器的数据格式
        contentType:'application/json',
        // 设置crsf_token防止跨站伪造访问
        headers:{
                'X-CSRFToken':getCookie('crsf_token')
            },
        // 请求成功后执行的回调函数
        success:function(response){
            if(response.errno=='0'){
                // 短信验证码显示倒计时，倒计时结束后才能重新获取短信验证码
                var num =60;
                // 设置一个计时器
                var timer = setInterval(function () {
                    if(num ==1){
                    // 倒计时结束，清除计时器，显示原来的内容并且重新设置点击事件
                    clearInterval(timer);
                    $('.get_code').html('点击获取验证码');
                    $('.get_code').attr('onclick','sendSMSCode()');
                    }else{
                    // 不满足条件就倒计时并显示在页面上
                        num-=1;
                        $('.get_code').html(num+'秒')
                    }
                },1000)
            }else{
                alert(response.error_info);
                $('.get_code').html('点击获取验证码');
                $('.get_code').attr('onclick','sendSMSCode()');
            }

        }

    })
}

// 调用该函数模拟点击左侧按钮
function fnChangeMenu(n) {
    var $li = $('.option_list li');
    if (n >= 0) {
        $li.eq(n).addClass('active').siblings().removeClass('active');
        // 执行 a 标签的点击事件
        $li.eq(n).find('a')[0].click()
    }
}

// 一般页面的iframe的高度是660
// 新闻发布页面iframe的高度是900
function fnSetIframeHeight(num) {
    var $frame = $('#main_frame');
    $frame.css({'height': num});
}

function getCookie(name) {
    var r = document.cookie.match("\\b" + name + "=([^;]*)\\b");
    return r ? r[1] : undefined;
}

function generateUUID() {
    var d = new Date().getTime();
    if (window.performance && typeof window.performance.now === "function") {
        d += performance.now(); //use high-precision timer if available
    }
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
    return uuid;
}


// 用户退出
function outlanding() {
    $.get('/register/outlanding',function () {
        location.reload()

    })


}