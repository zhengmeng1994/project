from flask import session
# 通过命令行的方法启动flask框架
from flask_script import Manager
from info import create_app, db, models
# 导入数据库迁移框架
from flask_migrate import Migrate, MigrateCommand

# 将定义的字典中的键作为参数传进工厂函数
app = create_app('develop')

manager = Manager(app)
# 设置数据库迁移框架
Migrate(app, db)
# 添加数据库迁移命令
manager.add_command('db', MigrateCommand)
if __name__ == "__main__":
    # app.run()
    # 设置启动脚本后依然可以用pycharm启动
    # 查看路由映射
    # print(app.url_map)
    manager.run()
